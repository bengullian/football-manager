﻿using Football.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Football.Core
{
    public static class TeamsTable
    {
        public static List<Match> GetMatchesPlayedLatestFirst(IContextProvider provider, string teamName)
        {
            List<Match> MatchList = (List<Match>) provider.GetAllMatches();
            List<Match> myMatches = MatchList.FindAll(m => m.HomeTeamName.Equals(teamName) || m.AwayTeamName.Equals(teamName));
            myMatches.Sort((x, y) => x.MatchDate.CompareTo(y.MatchDate));
            
            return myMatches;
        }
    }
}
