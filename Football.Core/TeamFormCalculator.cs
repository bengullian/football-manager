﻿using Football.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Football.Core
{
    public static class TeamFormCalculator
    {           

        // returns a list if chars to be used for the team form string
        public static List<char> TeamFormHome(string teamName, List<Match> Matches)
        {
            List<char> result = new List<char>();        
            Matches.ForEach(match => {
                if (match.HomeTeamScore > match.AwayTeamScore)
                {
                    result.Add('W');
                }

                 else if (match.HomeTeamScore < match.AwayTeamScore)
                {
                    result.Add('L');
                }

                else if (match.HomeTeamScore == match.AwayTeamScore)
                {
                    result.Add('D');
                }
            });

            return result;
        }


        public static List<char> TeamFormAway(string teamName, List<Match> Matches)
        {
            List<char> result = new List<char>();
            Matches.ForEach(match => {
                if (match.HomeTeamScore > match.AwayTeamScore)
                {
                    result.Add('L');
                }

                else if (match.HomeTeamScore < match.AwayTeamScore)
                {
                    result.Add('W');
                }

                else if (match.HomeTeamScore == match.AwayTeamScore)
                {
                    result.Add('D');
                }
            });

            return result;
        }
    }
}
