﻿using Football.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Football.Core
{
    public class ContextProvider : IContextProvider
    {
        private ITeamRepository _teamRepository;
        private IMatchesRepository _matchesRepository;

        public ContextProvider(ITeamRepository tr, IMatchesRepository mr)
        {
            _teamRepository = tr;
            _matchesRepository = mr;
        }

        public List<Team> GetAllTeams()
        {
            return _teamRepository.GetAllTeams();
        }


        public List<Match> FindAllMatches(Expression<Func<Match, bool>> predicate)
        {
            return _matchesRepository.Find(predicate);
        }


        public void AddTeam(Team team)
        {
            if (_teamRepository.Find(t => t.Name == team.Name).Count < 1)
            {
                _teamRepository.Add(team);
            }         
        }
       

        public void AddMatch(Match match)
        {
            if (_matchesRepository.Find(m => m.HomeTeamName == match.HomeTeamName && 
                    m.AwayTeamName == match.AwayTeamName && m.MatchDate == match.MatchDate).Count == 0)
                        _matchesRepository.Add(match);
        }


        public List<Match> GetAllMatches()
        {
            return _matchesRepository.GetAll();
        }


        public Team GetTeam(string stringName)
        {
            List<Team> teams = _teamRepository.GetAllTeams();
            
            return teams.Find(t => t.Name == stringName) ?? null;
        }
    }
}
