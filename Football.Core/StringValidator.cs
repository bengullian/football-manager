﻿using Football.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Football.Core
{
    public static class StringValidator 
    {
        public static bool ValidateMatchTeamNames(string homeTeamName, string awayTeamName)
        {
            return ValidateString(homeTeamName) && ValidateString(awayTeamName);
        }


        private static bool ValidateString(string inputString)
        {
            string[] name = inputString.Split(' ');

            foreach(string n in name)
            {
                if (Regex.Replace(n, "[^\\w\\.-]", "").Length != n.Length && n.Length > 1)
                {
                    return false;
                }                   
            }

            return true;
        }
    }
}
