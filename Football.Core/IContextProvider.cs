﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Football.Data;

namespace Football.Core
{
    public interface IContextProvider
    {
        List<Match> FindAllMatches(Expression<Func<Match, bool>> predicate);
        List<Team> GetAllTeams();
        void AddTeam(Team team);
        void AddMatch(Match match);
        Team GetTeam(string stringName);
        List<Match> GetAllMatches();
    }
}