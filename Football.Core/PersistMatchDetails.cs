﻿using Football.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Football.Core
{
    public static class PersistMatchDetails
    {
        public static void run(IContextProvider contextProvider, Match match)
        {        
            contextProvider.AddMatch(match);
        }
    }
}
