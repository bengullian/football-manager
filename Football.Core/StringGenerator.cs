﻿using Football.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Football.Core
{
    public static class StringGenerator
    {

        public static string GenerateTeamFormString(List<Match> listOfMatches, string teamName)
        {
            List<Match> homeMatches = listOfMatches.FindAll(m => m.HomeTeamName.Equals(teamName));
            List<Match> awayMatches = listOfMatches.FindAll(m => m.AwayTeamName.Equals(teamName));

            List<char> results = TeamFormCalculator.TeamFormHome(teamName, homeMatches);
            results.Add('#'); // A char to seperate home and away results 
            results.AddRange(TeamFormCalculator.TeamFormAway(teamName, awayMatches));

            string results2 = "";
            foreach (char c in results)
            {
                results2 += c;
            }

            return results2.ToString();
        }
    }
}
