﻿using Football.Core;
using Football.Data;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Football.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextPool<AppDbContext>(
                options => options.UseSqlServer(Configuration.GetConnectionString("DevelopmentDbConnection"))                
            );           
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddScoped<IContextProvider, ContextProvider>();
            services.AddScoped<IMatchesRepository, MatchesRepository>();
            services.AddScoped<ITeamRepository, TeamRepository>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection(); 
            
           
            app.UseMvc(routes =>
            {                
                
                routes.MapRoute(
                   name: "table",
                   template: "{controller=Home}/{action=TeamMatchTable}/{teamName}");

                routes.MapRoute(
                   name: "teamForm",
                   template: "{controller=Home}/{action=TeamForm}/{teamName}");

                routes.MapRoute(
                   name: "default",
                   template: "{controller=Home}/{action=Index}");

                routes.MapRoute(
                   name: "values",
                   template: "{controller=Values}/{action=Post}");
            });
        }
    }
}
