﻿using System;
using System.Collections.Generic;
using Football.Core;
using Football.Data;
using Match = Football.Data.Match;
using System.Globalization;
using Microsoft.AspNetCore.Mvc;

namespace Football.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]    
    public class MatchController : ControllerBase
    {
        private readonly IContextProvider _provider;
       

        public MatchController(IContextProvider cp) => _provider = cp;

        // POST api/values       
        [HttpPost]        
        public Object Post(Match match)
        {
            // validate the date
            string date = null;
            try
            {
                date = DateTime.ParseExact(match.MatchDate.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture).ToShortDateString();

            }
            catch (FormatException e) {}

            if (date == null)
            {
                return new { status = 200, error = "invalid date format, use dd/MM/yyyy" };
            }
            
            // validate team names
            if (StringValidator.ValidateMatchTeamNames(match.HomeTeamName, match.AwayTeamName))
            {
                if (_provider.GetTeam(match.HomeTeamName) == null)
                {
                    _provider.AddTeam(new Team(match.HomeTeamName));
                }

                if (_provider.GetTeam(match.AwayTeamName) == null)
                {
                    _provider.AddTeam(new Team(match.AwayTeamName));
                }
                
                // persist
                PersistMatchDetails.run(_provider, match);

                return new { status = 200, message = "Match creation succeeded" }; 
            }

            return new { status = 200, error = "invalid team name" };
        }


        // GET api/values
        [HttpGet]      
        public List<Match> Get()
        {
            return (List<Match>)_provider.GetAllMatches();
        }   
        

    }  

}

