﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Football.Core;
using Football.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Football.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatchTableController : ControllerBase
    {
        private readonly IContextProvider _provider;

        public MatchTableController(IContextProvider cp) => _provider = cp;

       
        [HttpGet]
        public List<Match> Table(string teamName)
        {
            return (List<Match>)TeamsTable.GetMatchesPlayedLatestFirst(_provider, teamName);

        }
    }
}