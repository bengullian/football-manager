﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Football.Core;
using Football.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Football.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamFormController : ControllerBase
    {
        private readonly IContextProvider _provider;

        public TeamFormController(IContextProvider cp) => _provider = cp;

 
        [HttpGet]
        public string TeamForm(string teamName)
        {
            List<Match> MatchList = _provider.GetAllMatches();
            return StringGenerator.GenerateTeamFormString(MatchList, teamName);

        }


    }
}