﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Football.Core;
using Football.Data;
using Microsoft.AspNetCore.Mvc;


namespace Football.API.Controllers
{
    public class HomeController : Controller
    {
        private readonly IContextProvider _provider;        
    

        public HomeController(IContextProvider cp)
        {
            _provider = cp;
        }
        

        public IActionResult Index()
        {
            //ViewBag.Teams = _provider.GetAllTeams().Select(T => T).Distinct().ToList();
            ViewBag.Teams = _provider.GetAllTeams().ToList();
            ViewBag.MatchList = (List<Match>)_provider.GetAllMatches();
            return View();
        }


        //to submit match results through a form use the api instead
        [HttpPost]
        public ActionResult Post(Match m)
        {
            new MatchController(_provider).Post(m);
            ViewBag.Teams = _provider.GetAllTeams().ToList();
            ViewBag.MatchList = (List<Match>)_provider.GetAllMatches();
            return View();
        }


        [HttpGet]
        public ActionResult TeamMatchTable(string teamName)
        {            
            ViewBag.Matches = TeamsTable.GetMatchesPlayedLatestFirst(_provider, teamName);
            return PartialView("SortedMatches");
        }


        [HttpGet]
        public string TeamForm(string teamName)
        {
            List<Match> MatchList = _provider.GetAllMatches();
            return StringGenerator.GenerateTeamFormString(MatchList, teamName);
        }
    }
}