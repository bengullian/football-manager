#pragma checksum "C:\Users\ben\source\repos\Football\Football.API\Views\Home\SortedMatches.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "7c36c1752b5b7afa1c7b49620cb9055d1bfc2e62"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_SortedMatches), @"mvc.1.0.view", @"/Views/Home/SortedMatches.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/SortedMatches.cshtml", typeof(AspNetCore.Views_Home_SortedMatches))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\SortedMatches.cshtml"
using Football.Data;

#line default
#line hidden
#line 2 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\SortedMatches.cshtml"
using System.Globalization;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"7c36c1752b5b7afa1c7b49620cb9055d1bfc2e62", @"/Views/Home/SortedMatches.cshtml")]
    public class Views_Home_SortedMatches : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(53, 346, true);
            WriteLiteral(@"
<table class=""table table-bordered"">
    <thead class=""thead-light"">
        <tr>
            <th scope=""col"" class=""th"">Match Day</th>
            <th scope=""col"" class=""th"">Home Team</th>
            <th scope=""col"" class=""th"">Result</th>
            <th scope=""col"" class=""th"">Away Team</th>
        </tr>
    </thead>
    <tbody>
");
            EndContext();
#line 14 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\SortedMatches.cshtml"
          
            if (ViewBag.Matches.Count > 0)
            {
                foreach (Match m in ViewBag.Matches)
                {
                    DateTime MyDateTime;

                    try
                    {
                        MyDateTime = DateTime.ParseExact(@m.MatchDate.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    }

                    catch (FormatException e)
                    {
                        MyDateTime = DateTime.ParseExact(@m.MatchDate.Trim(), "MM/dd/yyyy", CultureInfo.InvariantCulture);
                        
                    }


#line default
#line hidden
            BeginContext(1031, 65, true);
            WriteLiteral("                    <tr>\r\n                        <td class=\"td\">");
            EndContext();
            BeginContext(1097, 25, false);
#line 34 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\SortedMatches.cshtml"
                                  Write(MyDateTime.Day.ToString());

#line default
#line hidden
            EndContext();
            BeginContext(1122, 14, true);
            WriteLiteral("<sup>th</sup> ");
            EndContext();
            BeginContext(1137, 27, false);
#line 34 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\SortedMatches.cshtml"
                                                                          Write(MyDateTime.ToString("MMMM"));

#line default
#line hidden
            EndContext();
            BeginContext(1164, 46, true);
            WriteLiteral("</td>\r\n                        <td class=\"td\">");
            EndContext();
            BeginContext(1211, 14, false);
#line 35 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\SortedMatches.cshtml"
                                  Write(m.HomeTeamName);

#line default
#line hidden
            EndContext();
            BeginContext(1225, 46, true);
            WriteLiteral("</td>\r\n                        <td class=\"td\">");
            EndContext();
            BeginContext(1272, 26, false);
#line 36 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\SortedMatches.cshtml"
                                  Write(m.HomeTeamScore.ToString());

#line default
#line hidden
            EndContext();
            BeginContext(1298, 10, true);
            WriteLiteral("<b> - </b>");
            EndContext();
            BeginContext(1309, 26, false);
#line 36 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\SortedMatches.cshtml"
                                                                       Write(m.AwayTeamScore.ToString());

#line default
#line hidden
            EndContext();
            BeginContext(1335, 46, true);
            WriteLiteral("</td>\r\n                        <td class=\"td\">");
            EndContext();
            BeginContext(1382, 14, false);
#line 37 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\SortedMatches.cshtml"
                                  Write(m.AwayTeamName);

#line default
#line hidden
            EndContext();
            BeginContext(1396, 34, true);
            WriteLiteral("</td>\r\n                    </tr>\r\n");
            EndContext();
#line 39 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\SortedMatches.cshtml"
                }
            }
        

#line default
#line hidden
            BeginContext(1475, 26, true);
            WriteLiteral("    </tbody>\r\n</table>\r\n\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(1519, 2, true);
                WriteLiteral("\r\n");
                EndContext();
#line 46 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\SortedMatches.cshtml"
      await Html.RenderPartialAsync("_ValidationScriptsPartial");

#line default
#line hidden
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
