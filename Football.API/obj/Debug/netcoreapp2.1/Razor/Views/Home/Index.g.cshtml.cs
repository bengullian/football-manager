#pragma checksum "C:\Users\ben\source\repos\Football\Football.API\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "24924a56d9757d87c7cacaf1d8b858e6c0ac2529"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"24924a56d9757d87c7cacaf1d8b858e6c0ac2529", @"/Views/Home/Index.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 2 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Index";    

#line default
#line hidden
            BeginContext(47, 37, true);
            WriteLiteral("\r\n<!DOCTYPE html>\r\n<html lang=\"en\">\r\n");
            EndContext();
            BeginContext(84, 1090, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "24924a56d9757d87c7cacaf1d8b858e6c0ac25293156", async() => {
                BeginContext(90, 982, true);
                WriteLiteral(@"
    <meta name=""viewport"" content=""width=device-width, initial-scale=1, shrink-to-fit=no"" />
    <link rel=""stylesheet"" href=""https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"" integrity=""sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"" crossorigin=""anonymous"">
    <script src=""https://code.jquery.com/jquery-3.3.1.slim.min.js"" integrity=""sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"" crossorigin=""anonymous""></script>
    <script src=""https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"" integrity=""sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"" crossorigin=""anonymous""></script>
    <script src=""https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"" integrity=""sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"" crossorigin=""anonymous""></script>

    <environment include=""Development"">        
        <link");
                EndContext();
                BeginWriteAttribute("ref", " ref=", 1072, "", 1107, 1);
#line 16 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\Index.cshtml"
WriteAttributeValue("", 1077, Url.Content("~/css/site.css"), 1077, 30, false);

#line default
#line hidden
                EndWriteAttribute();
                BeginContext(1107, 60, true);
                WriteLiteral(" rel=\"stylesheet\" type=\"text/css\" />\r\n    </environment>\r\n\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(1174, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
            BeginContext(1178, 1191, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "24924a56d9757d87c7cacaf1d8b858e6c0ac25295845", async() => {
                BeginContext(1184, 655, true);
                WriteLiteral(@"
    <br />
    <div class=""row d-flex topBar"">
        <div class=""col-md-2 topBarlabel""><a id=""teamSelected"" style=""padding-left:5px;""></a></div>
        <div class=""col-md-3"">Home:<a id=""homeForm"" style=""padding-left:5px;""></a></div>
        <div class=""col-sm-3"" style=""text-align:center;"">Away:<a id=""awayForm"" style=""padding-left:5px;""></a></div>
        <div class=""col-sm-3"" style=""text-align:center;"">Total:<a id=""totalForm"" style=""padding-left:5px;""></a></div>
    </div>
    <br />
    <div class=""container-fluid d-flex"">

        <div id=""seasonTable"" style=""height:600px; overflow-y:scroll"" class=""col-md-6 border border-light"">
");
                EndContext();
#line 33 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\Index.cshtml"
              
                await Html.RenderPartialAsync("SeasonTable");
            

#line default
#line hidden
                BeginContext(1933, 113, true);
                WriteLiteral("        </div>\r\n        <div id=\"teamName\" style=\"height:600px; overflow-y:scroll\" class=\"col-md-3 list-group\">\r\n");
                EndContext();
#line 38 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\Index.cshtml"
              
                await Html.RenderPartialAsync("TeamList");
            

#line default
#line hidden
                BeginContext(2137, 48, true);
                WriteLiteral("        </div>\r\n        <div class=\"col-md-4\">\r\n");
                EndContext();
#line 43 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\Index.cshtml"
              
                await Html.RenderPartialAsync("MatchScoresForm");
                
            

#line default
#line hidden
                BeginContext(2301, 32, true);
                WriteLiteral("        </div>\r\n    </div>\r\n    ");
                EndContext();
                BeginContext(2334, 26, false);
#line 49 "C:\Users\ben\source\repos\Football\Football.API\Views\Home\Index.cshtml"
Write(Url.Content("~/js/app.js"));

#line default
#line hidden
                EndContext();
                BeginContext(2360, 2, true);
                WriteLiteral("\r\n");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(2369, 9, true);
            WriteLiteral("\r\n</html>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
