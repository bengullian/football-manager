﻿namespace Football.Data
{
    public interface ITeam
    {
        int Id { get; set; }
        string Name { get;  }

        int GoalsConceeded { get; }
        int GoalsScored { get; }

        
        int AddToGoalsConceeded(int score);
        int AddToGoalsScored(int score);
    }
}