﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;


namespace Football.Data
{
    public class Match : IMatch
    {
        // not setting the Teams: hometeam and awayTeam in the constructor to prevent duplicate teams
        // a new team will be created when the teamname does not already exist before persisting requests
        public Match(string homeTeamName, int homeTeamScore, string awayTeamName, int awayTeamScore, string matchDate)
        {
            
            HomeTeamName = homeTeamName;
            HomeTeamScore = homeTeamScore;

            AwayTeamName = awayTeamName; 
            AwayTeamScore = awayTeamScore;
            MatchDate = matchDate;
        }

        private Match() { }

        [Key]
        public String MatchDate { get; set; }

        [Key]
        public String HomeTeamName { get; set; }

        [Key]
        public String AwayTeamName { get; set; }

        public int HomeTeamScore { get; set; }

        public int AwayTeamScore { get; set; }
    }
}
