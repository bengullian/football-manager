﻿using System;

namespace Football.Data
{
    public interface IMatch
    {       
        int AwayTeamScore { get; set; }
        string HomeTeamName { get; set; }
        int HomeTeamScore { get; set; }
        string AwayTeamName { get; set; }
        string MatchDate { get; set; }
    }
}