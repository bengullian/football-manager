﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Football.Data
{
    public class Team : ITeam
    {
        public Team(string name)
        {
            Name = name;
            GoalsScored = 0;
            GoalsConceeded = 0;
        }

        public Team() {
            Name = "undefined";
            GoalsScored = 0;
            GoalsConceeded = 0;
        }

        [Key]
        public int Id { get; set; }

        
        public string Name { get;  set; }

        public int GoalsScored { get; private set; }

        public int GoalsConceeded { get; private set; }

        public int AddToGoalsScored(int score)
        {
            return GoalsScored += score;
        }

        public int AddToGoalsConceeded(int score)
        {
            return GoalsConceeded += score;
        }






    }
}
