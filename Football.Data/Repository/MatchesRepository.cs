﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Football.Data
{
    public class MatchesRepository : IMatchesRepository
    {
        protected readonly DbContext Context;
        private DbSet<Match> _matches;

        public MatchesRepository(AppDbContext context)
        {
            Context = context;
            _matches = Context.Set<Match>();
        }

        public void Add(Match match)
        {  
           //ensure that the same match doesn't already exist 
          IEnumerable<Match> old = _matches.Where(m => m.HomeTeamName.Equals(match.HomeTeamName) &&
                            m.AwayTeamName.Equals(match.AwayTeamName) &&
                            m.MatchDate == match.MatchDate );
            if (old.Count() == 0)
            {
                _matches.Add(match);
                Context.SaveChanges();
            }               
        }


        public List<Match> Find(Expression<Func<Match, bool>> predicate)
        {
            return (List <Match>) _matches.Where(predicate).ToList();
        }


        public Match Get(int id)
        {
            return _matches.Find(id);
        }

        public List<Match> GetAll()
        {             
            return _matches.ToList();   
        }


    }
}
