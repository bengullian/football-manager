﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Football.Data
{
    public class TeamRepository : ITeamRepository
    {
        protected readonly AppDbContext Context;
        private DbSet<Team> _teams;

        public TeamRepository(AppDbContext context)
        {
            Context = context;
            _teams = Context.Set<Team>();
        }     

        public void Add(Team team)
        { 
            _teams.Add(team);
            Context.SaveChanges();
        }

        public List<Team> Find(Expression<Func<Team, bool>> predicate)
        {
            IEnumerable<Team> t = _teams.Where(predicate);            
            return t.ToList();
        }


        public Team Get(int id)
        {
            return _teams.Find(id);
        }

        public List<Team> GetAllTeams()
        {
            return _teams.ToList();
        }

    }
}


