﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Football.Data
{
    public interface ITeamRepository
    {
        void Add(Team entity);
        List<Team> Find(Expression<Func<Team, bool>> predicate);
        Team Get(int id);
        List<Team> GetAllTeams();
    }
}