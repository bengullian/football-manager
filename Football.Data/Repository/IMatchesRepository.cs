﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Football.Data
{
    public interface IMatchesRepository
    {
        void Add(Match match);
        List<Match> Find(Expression<Func<Match, bool>> predicate);
        Match Get(int id);
        List<Match> GetAll();
    }
}