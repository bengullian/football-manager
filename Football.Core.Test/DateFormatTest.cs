﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Football.Core.Test
{
    public class DateFormatTest
    {
        [Theory]
        [InlineData("2019-05-29", "29 May")]
        public void FormatInputMatchDateString_Test(string inputDate, string expectedDate)
        {
            DateTime MyDateTime = DateTime.Parse(inputDate);
            String month = MyDateTime.ToString("MMMM");
            String day = MyDateTime.Day.ToString();
            Assert.Equal(expectedDate, day + " " + month);

        }

    }
}
