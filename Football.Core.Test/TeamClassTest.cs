﻿using Football.Data;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Football.Core.Test
{
    public class TeamClassTest
    {
        [Theory]
        [InlineData(0, 2, 2 )]
        public void AddToGoalsScored_ShouldCalculate_Test(int currentScore, int addedScore, int expectedScore)
        {
            Team team = new Team("Hoody");               
           int scored0 = team.GoalsScored;
           int scored1 = team.AddToGoalsScored(addedScore);

            Assert.Equal(scored0, currentScore);
            Assert.Equal(scored1, expectedScore);
        }


        [Theory]
        [InlineData(0, 2, 2)]
        public void AddToGoalsConceeded_ShouldCalculate_Test(int currentConceeds, int newConceed, int expectedTotal)
        {
            Team team = new Team("Robins");            
            int conceeds0 = team.GoalsConceeded;
            int conceeds1 = team.AddToGoalsConceeded(newConceed);

            Assert.Equal(conceeds0, currentConceeds);
            Assert.Equal(conceeds1, expectedTotal);
            
        }

    }
}
