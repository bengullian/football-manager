﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Xunit;

namespace Football.Core.Test
{
    public class CleanTeamNameTest
    {
        [Theory]
        [InlineData("male/@#=del", "maledel")]
        [InlineData("Everton", "Everton")]
        public void RemoveUnwantedChasFromTeamName_Test(string inputName, string expectedOutput)
        {
            Assert.Equal(expectedOutput, Regex.Replace(inputName, "[^\\w\\.-]", ""));
            Assert.Equal(7, Regex.Replace(inputName, "[^\\w\\.-]", "").Length);
        }
    }
}
